# Initial setup

```bash
python3 -m venv Notebooks-virtualenv
source Notebooks-virtualenv/bin/activate
python -m pip install numpy scipy matplotlib ipython jupyter pandas sympy nose
```

# Subsequent logins

```
source Notebooks-virtualenv//bin/activate
```